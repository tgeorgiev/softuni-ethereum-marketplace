//this function will be called when the whole page is loaded
window.onload = function(){
    if (typeof web3 === 'undefined') {
        //if there is no web3 variable
        displayMessage("Error! Are you sure that you are using metamask?");
    } else {
        displayMessage("Welcome to our marketplace!");
        init();
    }
}

var contractInstance;

var abi = [{"constant":false,"inputs":[{"name":"ID","type":"bytes32"},{"name":"quantity","type":"uint256"}],"name":"buy","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[],"name":"destroy","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"recipient","type":"address"}],"name":"destroyAndSend","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"name","type":"string"},{"name":"price","type":"uint256"},{"name":"quantity","type":"uint256"}],"name":"newProduct","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"new_owner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"ID","type":"bytes32"},{"name":"newQuantity","type":"uint256"}],"name":"update","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"withdraw","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":true,"name":"productId","type":"bytes32"},{"indexed":false,"name":"name","type":"string"},{"indexed":false,"name":"price","type":"uint256"},{"indexed":false,"name":"quantity","type":"uint256"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"ProductNew","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"productId","type":"bytes32"},{"indexed":false,"name":"initQuantity","type":"uint256"},{"indexed":false,"name":"newQuantity","type":"uint256"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"ProductUpdated","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"amount","type":"uint256"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"Withdraw","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"productId","type":"bytes32"},{"indexed":false,"name":"price","type":"uint256"},{"indexed":false,"name":"quantity","type":"uint256"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"ProductPurchased","type":"event"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"inputs":[],"payable":true,"stateMutability":"payable","type":"constructor"},{"constant":true,"inputs":[{"name":"ID","type":"bytes32"},{"name":"quantity","type":"uint256"}],"name":"getPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"ID","type":"bytes32"}],"name":"getProduct","outputs":[{"name":"name","type":"string"},{"name":"price","type":"uint256"},{"name":"quantity","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getProducts","outputs":[{"name":"","type":"bytes32[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getStoreBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"}];

var address = "0xc6b25d66c06b42cc14d6f12e50f8a8447594e9d9";
var acc;

function init(){
    var Contract = web3.eth.contract(abi);
    contractInstance = Contract.at(address);
    updateAccount();
}

function updateAccount(){
    //in metamask, the accounts array is of size 1 and only contains the currently selected account. The user can select a different account and so we need to update our account variable
    acc = web3.eth.accounts[0];
}

function displayMessage(message){
    var el = document.getElementById("message");
    el.innerHTML = message;
}

function getBalance() {
    updateAccount();
    //onReset();

    contractInstance.getStoreBalance.call({"from": acc}, function(err, res) {
        if (!err) {
            console.dir(res);
            displayMessage("Contract balance");
            document.getElementById("result").innerHTML = web3.fromWei(res, 'ether') + " ether(s)";
        } else {
            displayMessage("Something went wrong.");
            console.error(err);
        }
    });
}

function addProduct() {
    updateAccount();

    var input1 = document.querySelector('[name="name"]').value;
    var input2 = document.querySelector('[name="price"]').value;
    var input3 = document.querySelector('[name="quantity"]').value;

    contractInstance.newProduct(input1, input2, input3, {"from": acc}, function(err, res) {
        if (!err) {
            displayMessage("Success! Transaction hash: " + res.valueOf());
        } else {
            displayMessage("Something went wrong.");
            console.error(err);
        }
    });
}

function getProduct() {
    updateAccount();

    var input = document.getElementById('field1').value;

    if (input.startsWith("0x")) {
        contractInstance.getProduct.call(input, {"from": acc}, function(err, res) {
            if (!err) {
                document.querySelector('[name="name"]').value = res[0];
                document.querySelector('[name="price"]').value = web3.fromWei(res[1], 'ether') + " ether(s)";
                document.querySelector('[name="quantity"]').value = res[2];
                displayMessage("Product details");
            } else {
                displayMessage("Something went wrong.");
                console.error(err);
            }
        });
    } else {
        displayMessage("The input field does not contain a valid productId");
    }
}

function getProductData(productID) {

    contractInstance.getProduct.call(productID, {"from": acc}, function(err, res) {
        if (!err) {
            return res;
        } else {
            displayMessage("Something went wrong.");
            console.error(err);
        }
    });
}

function getAllProducts() {
    updateAccount();
    document.getElementById("products").innerHTML = "";

    contractInstance.getProducts.call({"from": acc}, function(err, res) {
        if (!err) {
            //displayProducts(res);
            displayProductsHTML(res);
        } else {
            displayMessage("Something went wrong.");
            console.error(err);
        }
    });
}

function displayProducts(productList) {
    var productArr = productList.toString().split(",");
    var output = "";

    for (var i=0; i<productArr.length; i++) {
        output = output.concat(productArr[i]) + "<br>";
    }

    var el = document.getElementById("products");
    el.innerHTML = output;s
}

function displayProductsHTML(productList) {
    var productArr = productList.toString().split(",");
    var output = "";
    var outputIDs = "";
    //console.dir(productArr.length);
    for (var i=0; i<productArr.length; i++) {
        outputIDs += '<li>'+productArr[i] + "</li>";
        var product = contractInstance.getProduct.call(productArr[i], {"from": acc}, function(err, res) {
            if (!err) {
                output =
                    '<div class="col-lg-4 col-md-6 mb-4">'+
                    '<div class="card h-80">'+
                    '<div class="card-body">'+
                    '<h4 class="card-title">'+
                    '<a href="#">'+res[0]+'</a>'+
                    '</h4>'+
                    '<div class="row">'+
                    '<div class="col-sm-6">' + res[1] + " ether(s)"+'</div>'+
                    '<div class="col-sm-6"><a href="javascript:;" class="btn btn-primary btn-sm float-right">Buy</a></div>'+
                    '<div class="col-sm-12">Qty: '+res[2]+'</div>'+
                    '<div class="col-sm-12" id="'+res[0]+'">ID: '+productArr[i]+'</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>';
                document.getElementById("products").innerHTML += output;
            } else {
                displayMessage("Something went wrong.");
                console.error(err);
            }
        });
        document.getElementById("productIDs").innerHTML = outputIDs;
    }
}

function getPrice() {
    updateAccount();

    var input1 = document.getElementById('field1').value;;
    var input2 = document.querySelector('[name="quantity"]').value;

    contractInstance.getPrice.call(input1, input2, {"from": acc}, function(err, res) {
        if (!err) {
            document.getElementById("result").innerHTML = web3.fromWei(res, 'ether') + " ether(s)";
        } else {
            displayMessage("Something went wrong.");
            console.error(err);
        }
    });
}

function buyProduct() {
    updateAccount();

    var input1 = document.querySelector('[name="name"]').value; // used for Product ID
    var input2 = document.querySelector('[name="price"]').value; // price given
    var input3 = document.querySelector('[name="quantity"]').value;


    contractInstance.buy(input1, input3, {"from": acc, value: web3.toWei(input2, 'ether')}, function(err, res) {
        if (!err) {
            displayMessage("Success! Transaction hash: " + res.valueOf());
        } else {
            displayMessage("Something went wrong.");
            console.error(err);
        }
    });
}

function withdraw() {
    updateAccount();

    contractInstance.withdraw({"from": acc}, function(err, res) {
        if (!err) {
            displayMessage("Success! Transaction hash: " + res.valueOf());
        } else {
            displayMessage("Something went wrong.");
            console.error(err);
        }
    });

}