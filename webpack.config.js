var webpack = require("webpack");
var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require('path');

module.exports = {
    entry: './app/js/script.js',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'script.js'
    },
    plugins: [
        // Copy our app's index.html to the build folder.
        new CopyWebpackPlugin([
            { from: './app/index.html', to: 'index.html' },
            { from: './app/css/shop-homepage.css', to: 'shop-homepage.css' },
            { from: './app/js/app.js', to: 'app.js' }
        ])
    ],
};