pragma solidity ^0.4.2;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Marketplace.sol";
import "../contracts/Basic/Owned.sol";
import "../contracts/Basic/SafeMath.sol";

contract TestMarketplaceUnit {

  // Truffle will send the TestContract one Ether after deploying the contract.
  uint public initialBalance = 1 ether;

  function () public {
    // This will NOT be executed when Ether is sent. \o/
  }

  /*
  function testInitialBalanceUsingDeployedContract() public {
    Marketplace market = Marketplace(DeployedAddresses.Marketplace());
    //Owned ownedContract = Owned(DeployedAddresses.Owned());
    //SafeMath mathContract = SafeMath(DeployedAddresses.SafeMath());


    uint expected = 0;

    Assert.equal(market.getStoreBalance(), expected, "Market should have 0 balance initially");
  }
  */

  function testInitialBalanceWithNewMarketplace() public {
    Marketplace market = new Marketplace();

    //uint expected = 0;
    Assert.equal(market.getStoreBalance(), 0, "Market should have 0 balance initially");
    //market.send(1);
    //Assert.equal(market.getStoreBalance(), 1, "Market should have 1 now");
  }

  function testAddingProduct() public {
    Marketplace market = new Marketplace();

    bytes32 ID = market.newProduct('item1',10,5);
    var (,price,quantity) = market.getProduct(ID);

    //Assert.equal(name, 'item1', "Product name should be item1"); // As of now solidity doesn't support returning strings between contracts. Because length of a string is not known at the time of call. So they only support fixed size arryas like bytes32.
    Assert.equal(price, 10, "Product price should be 10");
    Assert.equal(quantity, 5, "Product quantity should be 5");
  }

}
