const Marketplace = artifacts.require('./Marketplace.sol');

contract('Marketplace Integration Tests', async (accounts) => {

    let instance;
    let productId = "81742527ab632e6b2433c44f8a6b1f760201ed03dac3124c2626aec74b819aea"; // keccak256 for corsa

    beforeEach('new contract for each test', async function () {
        instance = await Marketplace.new();
    });

    it("contract balance can be viewed only by owner", async () => {
        try {
            await instance.getStoreBalance.call({from: accounts[1]});
            assert(false, "Only owner has permissions to call getStoreBalance()");
        } catch (error) {}
    });

    it("initial contract balance is 0", async () => {
        let balance = await instance.getStoreBalance.call({from: accounts[0]});
        assert.equal(balance.toNumber(), 0, "0 should be the balance of a new contract");
    });

    it("no products initially", async () => {
        let productIds = await instance.getProducts.call({from: accounts[0]});
        assert.equal(productIds.length, 0, "There should be zero products initially");
    });

    it("add two products and count them", async () => {
        await instance.newProduct("corsa", "1", "5", {from: accounts[0]});
        await instance.newProduct("vectra", "2", "10", {from: accounts[0]});
        let productIds = await instance.getProducts.call({from: accounts[0]});
        assert.equal(productIds.length, 2, "There should be two products");
    });

    it("get price of a product", async () => {
        await instance.newProduct("corsa", "1", "5");
        let details = await instance.getProduct(productId);
        let price = await instance.getPrice(productId, 3);
        let expectedPrice = details[1].toNumber() * 3;
        assert.equal(price.toNumber(), expectedPrice, "Prices should match");
    });

    it("add new product from owner", async () => {
        try {
            await instance.newProduct("corsa", "1", "5", {from: accounts[1]});
            assert(false, "Only contract owner can add new product");
        } catch (error) {}
    });

    /*
    it("add new product successfully", async () => {
        let result = await instance.newProduct("corsa", "1", "5");
        let details = await instance.getProduct(productId);
        assert.equal("corsa", details[0], "Name of the added product is not matching");
        assert.equal(web3.toWei(1), details[1].toNumber(), "Price of the added product is not matching");
        assert.equal("5", details[2].toNumber(), "Quantity of the added product is not matching");

        // checks for ProductNew event
        assert.equal(result.logs[0].event, "ProductNew", "There is no ProductAdded event");
    });
    */


});