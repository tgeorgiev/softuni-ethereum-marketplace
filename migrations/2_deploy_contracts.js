var Owned = artifacts.require("./Basic/Owned.sol");
var SafeMath = artifacts.require("./Basic/SafeMath.sol");
var Destructible = artifacts.require("./Basic/Destructible.sol");
var Marketplace = artifacts.require("./Marketplace.sol");

module.exports = function(deployer) {
  deployer.deploy(Owned);
  deployer.deploy(SafeMath);
  deployer.deploy(Destructible);
  deployer.link(Owned, Marketplace);
  deployer.link(SafeMath, Marketplace);
  deployer.link(Destructible, Marketplace);
  deployer.deploy(Marketplace);
};
