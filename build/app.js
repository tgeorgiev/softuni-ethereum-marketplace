//this function will be called when the whole page is loaded
window.onload = function(){
    if (typeof web3 === 'undefined') {
        //if there is no web3 variable
        displayMessage("Error! Are you sure that you are using metamask?");
    } else {
        displayMessage("Welcome to our DAPP!");
        init();
    }
}

var contractInstance;

var abi = [{"constant":false,"inputs":[{"name":"ID","type":"bytes32"},{"name":"quantity","type":"uint256"}],"name":"buy","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[],"name":"destroy","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"recipient","type":"address"}],"name":"destroyAndSend","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"name","type":"string"},{"name":"price","type":"uint256"},{"name":"quantity","type":"uint256"}],"name":"newProduct","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"new_owner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"ID","type":"bytes32"},{"name":"newQuantity","type":"uint256"}],"name":"update","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"withdraw","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":true,"name":"productId","type":"bytes32"},{"indexed":false,"name":"name","type":"string"},{"indexed":false,"name":"price","type":"uint256"},{"indexed":false,"name":"quantity","type":"uint256"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"ProductNew","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"productId","type":"bytes32"},{"indexed":false,"name":"initQuantity","type":"uint256"},{"indexed":false,"name":"newQuantity","type":"uint256"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"ProductUpdated","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"amount","type":"uint256"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"Withdraw","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"productId","type":"bytes32"},{"indexed":false,"name":"price","type":"uint256"},{"indexed":false,"name":"quantity","type":"uint256"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"ProductPurchased","type":"event"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"inputs":[],"payable":true,"stateMutability":"payable","type":"constructor"},{"constant":true,"inputs":[{"name":"ID","type":"bytes32"},{"name":"quantity","type":"uint256"}],"name":"getPrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"ID","type":"bytes32"}],"name":"getProduct","outputs":[{"name":"name","type":"string"},{"name":"price","type":"uint256"},{"name":"quantity","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getProducts","outputs":[{"name":"","type":"bytes32[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getStoreBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"}];

var address = "0xc6b25d66c06b42cc14d6f12e50f8a8447594e9d9";
var acc;

function init(){
    var Contract = web3.eth.contract(abi);
    contractInstance = Contract.at(address);
    updateAccount();
}

function updateAccount(){
    //in metamask, the accounts array is of size 1 and only contains the currently selected account. The user can select a different account and so we need to update our account variable
    acc = web3.eth.accounts[0];
}

function displayMessage(message){
    var el = document.getElementById("message");
    el.innerHTML = message;
}

function getBalance() {
    updateAccount();
    //onReset();

    contractInstance.getStoreBalance.call({"from": acc}, function(err, res) {
        if (!err) {
            displayMessage("Contract balance");
            document.getElementById("field1").value = web3.fromWei(res, 'ether') + " ether(s)";
        } else {
            displayMessage("Something went wrong.");
            console.error(err);
        }
    });
}
