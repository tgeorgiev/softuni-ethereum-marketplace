pragma solidity 0.4.19;

// File: contracts\Basic\Owned.sol

/**
    @notice Contract for maintaining ownership
*/
contract Owned {
  address public owner;

  /**
        @notice Default constructor
  */
  function Owned() public {
    owner = msg.sender;
  }

  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }

  /**
        @notice Changes the address of the store owner
        @param  new_owner Address of the new owner
  */
  function transferOwnership(address new_owner) public onlyOwner {
    if (new_owner != address(0) &&
        new_owner != owner) {
      owner = new_owner;
    }
  }

}

// File: contracts\Basic\Destructible.sol

contract Destructible is Owned {

  function Destructible() public payable { }

  // destroys the contract and sends the balance to the owner
  function destroy() public onlyOwner {
    selfdestruct(owner);
  }

  // destroys the contract and sends the balance to the specified recipient
  function destroyAndSend(address recipient) public onlyOwner {
    selfdestruct(recipient);
  }

}

// File: contracts\Basic\SafeMath.sol

/**
    @notice Safe mathematical operations contract
*/
library SafeMath {

  /**
        @notice Safely subtract two numbers without overflows
        @param x uint256 First operand
        @param y uint256 Second operand
        @return z Result
  */
  function safeSub(uint256 x, uint256 y) pure internal returns (uint256) {
    assert(y <= x);
    return x - y;
  }
  /**
        @notice Safely add two numbers without overflows
        @param x uint256 First operand
        @param y uint256 Second operand
        @return uint256 Result
  */
  function safeAdd(uint256 x, uint256 y) pure internal returns (uint256) {
    uint256 z = x + y;
    assert(z >= x);
    return z;
  }
  /**
        @notice Safely multiply two numbers without overflows
        @param x uint256 First operand
        @param y uint256 Second operand
        @return z uint256 Result
  */
  function safeMul(uint256 x, uint256 y) pure internal returns (uint256) {
    uint256 z = x * y;
    assert(x == 0 || z / x == y);
    return z;
  }
  /**
        @notice Safely divide two numbers without overflows
        @param x uint256 First operand
        @param y uint256 Second operand
        @return z uint256 Result
  */
  function safeDiv(uint256 x, uint256 y) pure internal returns (uint256) {
    uint256 z = x / y;
    return z;
  }

}

// File: contracts\Marketplace.sol

/**
    @notice This contract is a marketplace.
    @title Marketplace Contract
    @author Todor Georgiev
*/
contract Marketplace is Owned, Destructible {

    using SafeMath for uint256;

    mapping (bytes32 => Product) products;
    bytes32[] productsArr;
    uint256 productsCount;

    modifier available(bytes32 ID, uint256 quantity) {
        require(quantity > 0);
        require(products[ID].quantity >= quantity);
        _;
    }

    /* Events */
    event ProductNew(bytes32 indexed productId, string name, uint256 price, uint256 quantity, uint256 timestamp);
    event ProductPurchased(bytes32 indexed productId, uint256 price, uint256 quantity, uint256 timestamp);
    event ProductUpdated(bytes32 indexed productId, uint256 initQuantity, uint256 newQuantity, uint256 timestamp);
    event Withdraw(uint256 amount, uint256 timestamp);

    /**
        @notice Represents a product:
        Product name: @name
        Product price: @price
        Stock quantity: @quantity
    */
    struct Product {
        string name;
        uint256 price;
        uint256 quantity;
    }

    mapping (address => uint) pendingWithdrawal;

    /**
        @notice Default constructor
    */
    function Marketplace() public payable {

    }

    /**
        @notice Payable fallback to accept ETH into contract.
    */
    function() public payable {}

    /**
        @notice Buy a product. It should have enough quantity to be available.
    */
    function buy(bytes32 ID, uint quantity) public payable available(ID, quantity) {
        uint256 paymentSum = products[ID].price.safeMul(quantity);
        require(msg.value >= paymentSum);
        require(products[ID].quantity >= quantity);

        products[ID].quantity = products[ID].quantity.safeSub(quantity);

        ProductPurchased(ID, products[ID].price, quantity, now); //log event
    }

    /**
        @notice Updates product quantity. Only owner can do that.
    */
    function update(bytes32 ID, uint newQuantity) public onlyOwner {
        ProductUpdated(ID, products[ID].quantity, newQuantity, now); //log event
        products[ID].quantity = newQuantity;
    }

    /**
        @notice creates a new product and returns its ID. The ID is hash from the name
    */
    function newProduct(string name, uint price, uint quantity) public onlyOwner returns(bytes32) {

        require(price > 0);
        bytes32 ID = keccak256(name); //Generates ID (bytes32) from the name
        products[ID] = Product(name, price, quantity);

        productsArr.push(ID);
        productsCount++;
        ProductNew(ID, name, price, quantity, now); //log event
        return ID;
    }

    /**
        @notice get product data
    */
    function getProduct(bytes32 ID) public view returns(string name, uint price, uint quantity) {
        return (products[ID].name,
        products[ID].price,
        products[ID].quantity);
    }

    /**
        @notice get array with product IDs
    */
    function getProducts() public view returns(bytes32[]) {
        return productsArr;
    }

    /**
        @notice get product price
    */
    function getPrice(bytes32 ID, uint quantity) public view returns (uint) {
        return products[ID].price.safeMul(quantity);
    }

    /**
        @notice Returns stores's own balance
        @return balance Store's current balance
    */
    function getStoreBalance() onlyOwner constant public returns (uint256) {
        return this.balance;
    }

    /**
        @notice withdraw the contract funds. Only owner can do that.
    */
    function withdraw() public onlyOwner {
        require(this.balance > 0);
        pendingWithdrawal[msg.sender] = this.balance;
        uint256 amount = pendingWithdrawal[msg.sender];

        // zero the pending refund before sending to prevent re-entrancy attacks
        pendingWithdrawal[msg.sender] = 0;
        owner.transfer(amount);
        Withdraw(amount, now); //log event
    }
}
